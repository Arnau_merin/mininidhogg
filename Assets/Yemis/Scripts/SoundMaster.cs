﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMaster : MonoBehaviour
{
    public static AudioClip attackSound, deadSound, jumpSound, kickSound, throwSound;
    static AudioSource audioSrc;
    // Start is called before the first frame update
    void Start()
    {
        attackSound = Resources.Load<AudioClip> ("Attack");
        deadSound = Resources.Load<AudioClip> ("Dead");
        jumpSound = Resources.Load<AudioClip> ("Jump");
        kickSound = Resources.Load<AudioClip> ("Kick");
        throwSound = Resources.Load<AudioClip> ("Throw");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound (string clip)
    {
        switch(clip)
        {
            case "Attack":
            audioSrc.PlayOneShot(attackSound);
            break;
            case "Dead":
            audioSrc.PlayOneShot(deadSound);
            break;
            case "Jump":
            audioSrc.PlayOneShot(jumpSound);
            break;
            case "Kick":
            audioSrc.PlayOneShot(kickSound);
            break;
            case "Throw":
            audioSrc.PlayOneShot(throwSound);
            break;
        }
    }
}
