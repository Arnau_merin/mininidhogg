﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Play : MonoBehaviour
{
    public void Atras()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Empezar()
    {
        SceneManager.LoadScene("Map");
    }

    public void Controles()
    {
        SceneManager.LoadScene("Controls");
    }

    public void Creditos()
    {
        SceneManager.LoadScene("Creditos");
    }

    public void Salir()
    {
        Application.Quit();
    }
}
