﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private Rigidbody2D myRigidbody; // permite al script utilizar el Rigidbody 2D del personaje
    [SerializeField]
    private float movementSpeed; //velocidad del jugador caminando

    private bool facingRight;// indica si esta mirando a la derecha

    [SerializeField]
    private Transform[] groundPoints;

    [SerializeField]
    private float groundRadius;

    [SerializeField]
    private LayerMask whatIsGround;

    private bool isGrounded;

    private bool jump;

    [SerializeField]
    private float jumpForce;

    void Start()
    {
        facingRight = true; // Empieza mirando a la derecha
        myRigidbody = GetComponent<Rigidbody2D>(); // Referencia al Rigidbody 2D del player
    }
    void Update()
    {
        HandleInput();
        Debug.Log(myRigidbody.gravityScale);
    }
    void FixedUpdate() // FixedUpdate para que vaya igual en todos los pc
    {
        isGrounded = IsGrounded();
        float horizontal = Input.GetAxisRaw("Horizontal");
        HandleMovement(horizontal);
        Flip(horizontal);
        ResetValues();
    }

    private void ResetValues()
    {
        jump = false;
    }

    private void HandleMovement(float horizontal)
    { // void de movimiento de derecha a izquierda
        Vector2 move = new Vector3(movementSpeed * Mathf.Abs(horizontal), 0);
        myRigidbody.velocity = new Vector2(horizontal *movementSpeed, myRigidbody.velocity.y);        
        if (isGrounded && jump)
        {
            isGrounded = false;
            myRigidbody.AddForce(Vector2.up * 170);  
        }
    }

    private void HandleInput()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }
    }

    private void Flip(float horizontal)
    { // void que cambia la dirreción del personaje

        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;

            theScale.x *= -1; // si multiplicas -1 * 1 o al reves te da el contrario por lo cual el scale cambia

            transform.localScale = theScale;
        }
    }

    private bool IsGrounded()
    {
        if (myRigidbody.velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        myRigidbody.gravityScale = 1;
                        return true;
                    }

                }
            }
        }
        myRigidbody.gravityScale = 1;
        return false;
    }
}
