﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;

public class Player1 : MonoBehaviour
{
    public GameObject bulledPrefab;
    public GameObject hitPrefab;
    public GameObject jumpHitPrefab;
    public GameObject blood;
    public Transform spawnPlayer1;
    private Player2 thePlayer2;
    private Rigidbody2D myRigidbody; // permite al script utilizar el Rigidbody 2D del personaje
    private Animator animator;
    [SerializeField]
    private float movementSpeed, resetLevel = 0; //velocidad del jugador caminando
    public bool facingRight;// indica si esta mirando a la derecha
    [SerializeField]
    private Transform[] groundPoints;
    [SerializeField]
    private float groundRadius, timeToSpawn = -0.1f;
    [SerializeField]
    private LayerMask whatIsGround;  
    public bool isGrounded, imWining = false, imBlocking = false;
    private bool jump, canShoot, up, canBlock = true;
    int timeLeft = 0;
    [SerializeField]
    private float jumpForce, horizontal, timerShoot = 1;
    float spawnXPoint;
    public bool isAttacking;
    public GameObject Ply1Win;
    Vector3 PosDie;
    public int playerid = 0;
    public Rewired.Player player { get { return ReInput.isReady ? ReInput.players.GetPlayer(playerid) : null; } }




    void Start()
    {
        thePlayer2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<Player2>();
        animator = GetComponentInChildren<Animator>();
        facingRight = true; // Empieza mirando a la derecha
        myRigidbody = GetComponent<Rigidbody2D>(); // Referencia al Rigidbody 2D del player
        isAttacking = false;        
    }

    void Update()
    {
        HandleInput();
    }

    void FixedUpdate() // FixedUpdate para que vaya igual en todos los pc
    {
        isGrounded = IsGrounded();        
            //horizontal = Input.GetAxisRaw("HorizontalPlayer1");
            horizontal = player.GetAxis("HorizontalPlayerRew1");
            HandleMovement(horizontal);
            Flip(horizontal);
            if (horizontal != 0)
            {
                animator.SetBool("Run", true);
            }
            else if (horizontal == 0)
            {
                animator.SetBool("Run", false);
            }        
        ResetValues();
        Timer();
    }
    void Timer(){
        if(!canShoot){
            timerShoot -= Time.deltaTime;
            if(timerShoot <= 0){
                canShoot = true;                
                timerShoot = 1;
            }
        }
        if(timeToSpawn > 0){
            timeToSpawn -= Time.deltaTime;
            if(timeToSpawn <= 0){
                Respawn();
            }
        }
        if(resetLevel > 0)
        {
            resetLevel -= Time.deltaTime;
            if (resetLevel <= 0)
            {
                SceneManager.LoadScene("Map");
            }
        }
    }

    private void ResetValues()
    {
        jump = false;        
    }

    private void HandleMovement(float horizontal = 0)
    { // void de movimiento de derecha a izquierda
        if(imBlocking){
            animator.SetBool("Blocking", true);
            movementSpeed = 0;
            Vector2 move = new Vector3(0 * Mathf.Abs(0), 0);
            myRigidbody.velocity = new Vector2(0 * 0, myRigidbody.velocity.y);            
        }else{  
            animator.SetBool("Blocking", false);          
            movementSpeed = 2;
            Vector2 move = new Vector3(movementSpeed * Mathf.Abs(horizontal), 0);
            myRigidbody.velocity = new Vector2(horizontal * movementSpeed, myRigidbody.velocity.y);
            if (isGrounded && jump)
            {
                imBlocking = false;
                isGrounded = false;
                animator.SetBool("Jump", true);
                myRigidbody.AddForce(Vector2.up * 170);
            }
        }        
    }

    private void HandleInput()//Estoy mamadisimo xD, y mi coche carmadisimo esta to guapo el teclado bro 
    {
        //if(Input.GetKey("w")){ up = true; }else{up = false; canBlock = false;}
        if(player.GetButton("Block")){ up = true; }else{up = false; canBlock = false;}
        if(up && canBlock) { imBlocking = true; } else { imBlocking = false; }
        if (canShoot) { canBlock = true; }
        if (player.GetButtonDown("Jump1")){
            if (isGrounded == true && !up)
            {
                SoundMaster.PlaySound ("Jump");
            }
            jump = true;
        }
        timeLeft = timeLeft - 1;
        if (player.GetButtonDown("Atk") && !up && isGrounded)
        {
            SoundMaster.PlaySound ("Attack");
            AtkMelee();
            canBlock = false;
            // hit.GetComponent<BoxCollider2D> ().enabled = true;
            timeLeft = 10;
            isAttacking = true;
            animator.SetBool("Attacking", true);
        }
        if (timeLeft <= 0)
        {            
            animator.SetBool("Attacking", false);
            animator.SetBool("JumpAtk", false);
            isAttacking = false;
        }
        if(player.GetButtonDown("Atk") && !up && !isGrounded){    
            SoundMaster.PlaySound ("Kick");        
            AtkJump();
            canBlock = false;
            timeLeft = 10;
            animator.SetBool("JumpAtk", true);
        }        
        if(player.GetButtonDown("Atk") && canShoot && up){
            SoundMaster.PlaySound ("Throw");
            AtkRange();
            canBlock = false;
            canShoot = false;
        }
    }

    private void Flip(float horizontal)
    { // void que cambia la dirreción del personaje
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;
            Vector3 theScale = transform.localScale;
            theScale.x *= -1; // si multiplicas -1 * 1 o al reves te da el contrario por lo cual el scale cambia
            transform.localScale = theScale;
        }
    }

    private bool IsGrounded()
    {
        if (myRigidbody.velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        myRigidbody.gravityScale = 1;
                        animator.SetBool("Jump", false);
                        //animator.SetBool("JumpAtk", false);
                        return true;
                    }
                }
            }
        }
        myRigidbody.gravityScale = 1;
        return false;
    }
    void OnCollisionEnter2D(Collision2D collision)
    {  
        if (collision.gameObject.tag == "notPass2"){
            ImDead(true);                                    
        }
        if(collision.gameObject.tag == "Water"){
            Debug.Log("HOLA");
            ImDead(true);           
        }          
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "notPass2"){
            ImDead(true);
        }
        if (other.gameObject.tag == "Win1")
        {
            Ply1Win.SetActive(true);
            resetLevel = 1f;
            Debug.Log("PLAYER 1 WIN");
            
        }
    }
    public void AtkRange(){        
        if(facingRight){spawnXPoint = 0.3f;}else{spawnXPoint = -0.3f;}        
        Vector3 spawnPoint = new Vector3(transform.position.x + spawnXPoint, transform.position.y, 0);
        GameObject Bulled = Instantiate(bulledPrefab, spawnPoint, transform.rotation);
    }
    void AtkMelee(){ // Con el tag not pass no te puedes mover pero matas al otro
        if(facingRight){spawnXPoint = 0.3f;}else{spawnXPoint = -0.3f;}        
        Vector3 spawnPoint = new Vector3(transform.position.x + spawnXPoint, transform.position.y, 0);
        GameObject Melee = Instantiate(hitPrefab, spawnPoint, transform.rotation);        
    }
    void AtkJump(){ 
        if(facingRight){
            Debug.Log("JUMP ATK RIGHT");            
            //Vector2 jumpKick = Vector2.down * 250 +  Vector2.right * 550;
            Vector2 jumpKick = new Vector2(700, -200);
            myRigidbody.AddForce(jumpKick);
        }
        else{
            Debug.Log("JUMP ATK LEFT");            
            //Vector2 jumpKick = Vector2.down * 250 +  Vector2.left * 500;
            Vector2 jumpKick = new Vector2(-700, -200);
            myRigidbody.AddForce(jumpKick);
        }        
        Vector3 spawnPoint = new Vector3(transform.position.x, transform.position.y, 0);
        GameObject JumpAtk = Instantiate(jumpHitPrefab, spawnPoint, transform.rotation);        
    }
    
    public void ImDead(bool imDead){
        if(imDead){
            SoundMaster.PlaySound ("Dead");
            PosDie = new Vector3(transform.position.x, transform.position.y, 0);
            SpawnBlood();
            imWining = false;
            thePlayer2.imWining = true;
            transform.position = new Vector3(2.75f, 5, 0);
            timeToSpawn = 1.5f;
            imDead = false;
        }        
    }
    public void Respawn(){        
        transform.position = new Vector3(spawnPlayer1.position.x + 1, spawnPlayer1.position.y, 0);
    }
    void SpawnBlood()
    {
        GameObject Blood = Instantiate(blood, PosDie, transform.rotation);
    }
}

