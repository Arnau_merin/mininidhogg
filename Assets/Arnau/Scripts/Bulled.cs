﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulled : MonoBehaviour
{
    private Rigidbody2D rgb2d; 
    private GameObject player;
    private Player1 thePlayer;   
    private Player2 thePlayer2; 
    void Start()
    {
        rgb2d = GetComponent<Rigidbody2D> ();
        thePlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Player1>();
        thePlayer2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<Player2>();
        if(thePlayer.facingRight){
            Vector2 direciton = new Vector2(5, 0);
            rgb2d.AddForce(direciton * 20);            
        }else{
            Vector2 direciton = new Vector2(-5, 0);
            rgb2d.AddForce(direciton * 20);
            Vector3 theScale = transform.localScale;

            theScale.x *= -1; // si multiplicas -1 * 1 o al reves te da el contrario por lo cual el scale cambia

            transform.localScale = theScale;
        }
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor"){
            GameObject.Destroy(gameObject);                                   
        }   
        if (collision.gameObject.tag == "notPass1" || collision.gameObject.tag == "notPass2"){
            GameObject.Destroy(gameObject);                                   
        }
        if(collision.gameObject.tag == "Player2" && thePlayer2.imBlocking == false)
        {
            GameObject.Destroy(gameObject);
            thePlayer2.ImDead(true);            
        }
        if (collision.gameObject.tag == "Player2" && thePlayer2.imBlocking == true)
        {
            GameObject.Destroy(gameObject);
            thePlayer2.AtkRange();
        }
    }
}
