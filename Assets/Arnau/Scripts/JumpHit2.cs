﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpHit2 : MonoBehaviour
{
    float timerAlive = 0.2f;
    public Transform player2;
    public Player2 thePlayer2;
    void Start()
    {
        player2 = GameObject.FindWithTag("Player2").GetComponent<Transform>();
        thePlayer2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<Player2>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player2.position.x, player2.position.y, 0);
        timerAlive -= Time.deltaTime;
        if(timerAlive <= 0){
            Destroy(gameObject);
        }
    }
}
