﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    int speed = 2;
    float moveH;
    bool canJump = true;
    private Animator animator;
    private Rigidbody2D rb2d;
    bool flier = false;
    
    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        rb2d = GetComponent<Rigidbody2D> ();
    }

    // Update is called once per frame
    void Update()
    {
        moveH = Input.GetAxisRaw("Horizontal");   

        if(Input.GetKeyDown("space") && canJump){            
            Jump();              
        }          

        //Animation
        if(moveH != 0){
            animator.SetBool("Run", true);
        }else if(moveH == 0){
            animator.SetBool("Run", false);
        }
        

        
    }
    
    void FixedUpdate()
    {
        Vector2 move = new Vector3(speed * Mathf.Abs(moveH), 0);
        rb2d.velocity = new Vector2(moveH *speed, rb2d.velocity.y);        

        if(moveH < 0){             
            transform.eulerAngles = new Vector3(0, 180 ,0);
        }else if ( moveH > 0){
            transform.eulerAngles = new Vector3(0, 0 ,0);
        }
    }

    void Jump(){
        Debug.Log("Salto!");
        rb2d.AddForce(Vector2.up * 170);  
        animator.SetBool("Jump", true);        
        canJump = false;
    }
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Floor"){
            canJump = true;
            Debug.Log(canJump); 
            //animator.SetBool("Jump", false);
        }     
    }
}
