﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpHit : MonoBehaviour
{
    float timerAlive = 0.2f;
    public Transform player1;
    public Player1 thePlayer1;
    void Start()
    {
        player1 = GameObject.FindWithTag("Player").GetComponent<Transform>();
        thePlayer1 = GameObject.FindGameObjectWithTag("Player").GetComponent<Player1>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player1.position.x, player1.position.y, 0);
        timerAlive -= Time.deltaTime;
        if(timerAlive <= 0){
            Destroy(gameObject);
        }
    }
}
