﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform player1;
    public Player1 thePlayer1;
    public Transform player2;
    public Player2 thePlayer2;
    bool win = true;
    float smooth = 0.1f;
    Vector3 Offset;
    public GameObject Go1;
    public GameObject Go2;

    
    void Start()
    {
        player1 = GameObject.FindWithTag("Player").GetComponent<Transform>();
        player2 = GameObject.FindWithTag("Player2").GetComponent<Transform>();
        thePlayer2 = GameObject.FindGameObjectWithTag("Player2").GetComponent<Player2>();
        thePlayer1 = GameObject.FindGameObjectWithTag("Player").GetComponent<Player1>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(thePlayer2.imWining){
            Offset = new Vector3(player2.position.x, 1.2f, -10);
            Vector3 newPos = Offset;
            Vector3 smoothDirection = Vector3.Lerp(transform.position, newPos, smooth);
            transform.position = smoothDirection;
            Go2.SetActive(true);
            Go1.SetActive(false);
        }
        if (thePlayer1.imWining)
        {
            Offset = new Vector3(player1.position.x, 1.2f, -10);
            Vector3 newPos = Offset;
            Vector3 smoothDirection = Vector3.Lerp(transform.position, newPos, smooth);
            transform.position = smoothDirection;
            Go1.SetActive(true);
            Go2.SetActive(false);
        }
        //if(thePlayer1.imWining){ transform.position = new Vector3(player1.position.x, 1.2f, -10); }     
    }
}
